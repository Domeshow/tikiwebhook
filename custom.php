<?php

/**
 * @package tikiwiki
 */

// (c) Copyright by authors of the Tiki Wiki CMS Groupware Project
//
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.
// $Id$

function mautic_webhook_handler($args) {
    $request = json_decode(file_get_contents('php://input'), true);
    if ($request["mautic.form_on_submit"]) {
        $data = $request["mautic.form_on_submit"][0]["submission"]["results"];

        $input = [
            'source' => 'Lab 14 labserver',
            'name' => $data['instance_name'],
            'domain'   => $data['domain'],
            'email' => $data['email'],
            'branch' => 'master',
            'php_version' => '7.4',
        ];
        $jitParams = new JitFilter($input);

        // Calling the function fonction createVirtualminInstance
        $result = (new Services_Manager_Controller)->action_virtualmin_create($jitParams);
        
        if ($result['info']) {
            $mail = new TikiMail();
            $mail->setSubject(tra('Webhook Instance Creation'));
            $mail->setText(tra('Tiki email from:') . ' ' . $_SERVER['SERVER_NAME']);
            $mail->send($input['email']);
        }
    }
}
TikiLib::lib('events')->bind('tiki.webhook.received', 'mautic_webhook_handler');
